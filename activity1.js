//1. Add the following users:
db.users.insertMany ([
	{
		"firstName": "Diane",
		"lastName": "Murphy",
		"email": "dmurphy@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Mary",
		"lastName": "Patterson",
		"email": "mpatterson@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Jeff",
		"lastName": "Firrelli",
		"email": "jfirrelli@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Gerard",
		"lastName": "Bondur",
		"email": "gbondur@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Pamela",
		"lastName": "Castillo",
		"email": "pcastillo@mail.com",
		"isAdmin": true,
		"isActive": false
	},
	{
		"firstName": "George",
		"lastName": "Vanauf",
		"email": "gvanauf@mail.com",
		"isAdmin": true,
		"isActive": true
	}	
])

//2. Add the following courses
db.courses.insertMany([
	{
		"name": "Professional Development",
		"price": 10000.0
	},
	{
		"name": "Business Processing",
		"price": 13000.0
	}
])

//3. Get the users who are not administrators
db.users.find(
	{
		"isAdmin":false
	}
)


//4. Get user IDs and add them as enrollees of the courses (update)
db.courses.updateMany(
		{
			"name": "Professional Development"
		},
		{
			$set: {
				"enrollees":["userId","userId"]: ['ObjectId("620cc1e43fb456b9c9b04445")','ObjectId("620cc1e43fb456b9c9b04446")']

			}
		}
)

db.courses.updateOne(
		{
			"name": "Professional Development"
		},
		{
			$set: {
				"enrollees":[
						{
							"userId":ObjectId("620cc1e43fb456b9c9b04445")
						},
						{
							"userId":ObjectId("620cc1e43fb456b9c9b04446")
						}
					]
			}
		}
)