//2. New collection of users
db.users.insertMany ([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76.0,
		"email": "stephenhawking@mail.com",
		"department": "HR"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82.0,
		"email": "neilarmstrong@mail.com",
		"department": "HR"
	},	
	{
		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65.0,
		"email": "billgates@mail.com",
		"department": "Operations"
	},
	{
		"firstName": "Jane",
		"lastName": "Doe",
		"age": 21.0,
		"email": "janedoe@mail.com",
		"department": "HR"
	}		
])

//3. Find users with letter s in their first name or d in their last name
// $or and show only firstName and lastName, hide _id
db.users.find(
	{
		$or: [
				{
					"firstName": {
						$regex: 's',
						$options: '$i'
					}
				},
				{
					"lastName": {
						$regex: 'd',
						$options: '$i'
					}
				}
		]
	},
	{
		"_id": 0,
		"firstName":1,
		"lastName":1		
	}
)


//4. Find user from HR and age>=70
//$and
db.users.find (
	{
		$and:[
				{
					"department": "HR"
				},
				{
					"age": { $gte: 70}
				}
		]
	}
)

//5. Find users with letter e in their firstName and age<=30
//$and, $regex, $lte
db.users.find (
	{
		$and:[
				{
					"firstName": {
						$regex: 'e',
						$options: '$i'
					}
				},
				{
					"age": { $lte: 30}
				}
		]
	}
)

